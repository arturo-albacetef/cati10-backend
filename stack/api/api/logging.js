
const util = require('util');

module.exports = { requestLogging, serverLogging };

function requestLogging(event, tags) {
  const blacklisted = [
    'logs',
    'response',
    'pre',
    'preResponses',
    'server',
    'jsonp',
    'route',
    'raw'
  ];

  const filteredKeys = Object
    .keys(event)
    .filter( prop =>
      (!prop.startsWith('_')) &&
      (blacklisted.indexOf(prop) === -1)
    );

  const ev = {};
  for(let prop of filteredKeys) {
    ev[prop] = event[prop];
  }

  let msg = '\n' +
  'Request log:\n' +
  `  event: ${JSON.stringify(ev, null, 2)} \n` +
  `  tags: \n${JSON.stringify(tags, null, 2)}\n` +
  '';

  // console.log(msg);
}

function serverLogging(event, tags) {
  let msg = '\n' +
  'Server log:\n' +
  `  event: ${util.inspect(event)} \n` +
  `  tags: \n${JSON.stringify(tags, null, 2)}\n` +
  '';
  console.log(msg);
}
