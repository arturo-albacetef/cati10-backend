const crypto = require('crypto');

module.exports = function(models) {
  const { Agents, BonusCodes } = models;
  return {
    getAgents,
    getAgentRoles,
    updateAgent,
    unassignJobs,
    assignJobs,
    addNewAgent,
    updatePassword
  };

  async function getAgents() {
    const agents = await Agents
      // fetch main agent data
      .query()
      // fetch additional agent information
      .eager('jobs')
      .modifyEager('jobs', builder => {
        builder.where('is_active', '=', 1)
      })
      .eager('jobs.bonusCode');

    return agents;
  }

  async function getAgentRoles(roleIDs) {
    const { AgentRoles } = models;

    let agentRoles;
    if ( Array.isArray(roleIDs) ) {
      agentRoles = await AgentRoles
        .query()
        .where('id', 'in', roleIDs);
    } else {
      agentRoles = await AgentRoles.query();
    }

    return agentRoles;
  }

  async function addNewAgent(name, password, agentRoleID, extension) {
    const { Agents } = models;
    const salt = crypto.randomBytes(5).toString("hex");

    const agent = await Agents
      .query()
      .insert({
        name,
        pass: encrypt(password, salt),
        agent_role_id: agentRoleID,
        extension
      }, ['id']);

    return agent;
  }

  async function updateAgent(id, name, agentRoleID, extension) {
    const { Agents, AgentRoles } = models;

    const agents = await Agents
      .query()
      .patchAndFetchById(id, {
        name,
        agent_role_id: agentRoleID,
        extension
      });

    console.log('update agent with id', id, agents);
    return agents;
  }

  async function assignJobs(agentID, jobs) {
    const { Agents } = models;

    const name = await Agents
      .query()
      .select('name')
      .where('id', '=', agentID);

    const rows = jobs.map( j => ({
        agent_id: agentID,
        job_id: j.id,
        jobno: j.jobno,
        name
      })
    );

    const knex = Agents.knex();
    const data = await knex('c10_assigned_jobs')
      .insert(rows);

    return data;
  }

  async function unassignJobs(agentID, jobIDs) {
    const { Agents } = models;

    const knex = Agents.knex();

    const data = await knex('c10_assigned_jobs')
      .update({ is_active: 0 }, ['id', 'job_id', 'agent_id'])
      .where('agent_id', '=', agentID)
      .where('job_id', 'in', jobIDs)
      .where('is_active', '=', 1);

    return data;
  }

  async function updatePassword(agentID, oldPass, newPass) {

    const p = await Agents
      .query()
      .select()
      .where({ id: agentID });

    if ( p.length === 0 ) {
      throw new Error('No user found');
    }

    const currPassHash = p[0].pass;
    const salt = p[0].salt || ''; // cast null to empty string

    console.log('currPassHash', currPassHash);
    console.log('salt', salt);
    const oldPassHash = encrypt(oldPass, salt);

    if ( oldPassHash !== currPassHash ) {
      throw new Error('Invalid password');
    }

    console.log('newPass', newPass);
    const newPassHash = encrypt(newPass, salt);

    const agent = await Agents
      .query()
      .patchAndFetchById(agentID, { pass: newPassHash });

    return agent;
  }

  function encrypt(text, salt) {
    let c = crypto.createHash("sha1");
    c.update(text);
    c.update(salt);
    return c.digest("hex").toString();
  }


}
