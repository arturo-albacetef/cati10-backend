module.exports = function(models) {
  async function getQuestions() {
    const { Questions } = models;

    const questions = await Questions
      .query()
      .eager('[job]');

    return jobs;
  }


  async function getPDS() {
    const { PDSGroups } = models;

    const pds = await PDSGroups
      .query()
      .where('is_active', 1)
      .eager('[others, items, job]')
      .modifyEager('items', builder => builder.where('is_active', 1) );

    return pds;
  }

  async function getPDSJobs(pdsGroupID) {
    const { PDSGroups } = models;

    const pds = await PDSGroups
      .query()
      .where('id', '=', pdsGroupID)
      .eager('[jobs]')
  }

  async function addNewPDS(pdsData) {
    const { PDSGroups } = models;
    const { name, text, comment } = pdsData.pds;

    const pdsGroup = await PDSGroups
      .query()
      .insertAndFetch({ name, text, comment });

    return pdsGroup;
  }

  async function updatePDS(pdsGroupID, pdsData) {
    const { PDSGroups } = models;
    const { name, text, comment } = pdsData.pds;

    const pdsGroup = await PDSGroups
      .query()
      .patchAndFetchById(pdsGroupID, {name, text, comment});

    return pdsGroup;
  }

  /* @NOTE doesn't delete but rather archive */
  async function deletePDS(pdsGroupID) {
    const { PDSGroups } = models;

    const pdsGroup = await PDSGroups
      .query()
      .patchAndFetchById(pdsGroupID, { is_active: 0 });

    return pdsGroup;
  }

  async function addPDSItem( pdsItemData ) {
    const { PDSItems } = models;
    const { name, attribute_text, group_id } = pdsItemData;

    const pdsItem = await PDSItems
      .query()
      .insertAndFetch({ name, attribute_text, group_id });

    return pdsItem;
  }

  async function updatePDSItem( pdsItemID, pdsItemData ) {
    const { PDSItems } = models;
    const { attribute_text } = pdsItemData;

    const pdsItem = await PDSItems
      .query()
      .patchAndFetchById( pdsItemID, { attribute_text });

    return pdsItem;

  }
  async function deletePDSItem(pdsItemID) {
    const { PDSItems } = models;

    const pdsItem = await PDSItems
      .query()
      .patchAndFetchById(pdsItemID, { is_active: 0 });

    return pdsItem;
  }

  async function getPDSGroupUnassigned(pdsGroupID) {
    const { PDSOthers } = models;

    const data = await PDSOthers
      .query()
      .select()
      .where({ pds_group_id: pdsGroupID, is_assigned: 0 });

    return data;
  }

  async function getAllUnassigned() {
    const { PDSOthers } = models;

    const data = await PDSOthers
      .query()
      .select();

    return data;
  }

  async function assignEntry(pdsOtherID, pdsItemID, isError) {
    const { PDSOthers, PDSItems, PDSGroups, PDSAssignments } = models;

    // pull pds other so we can get its pds group ID
    const pdsOther = await PDSOthers
      .query()
      .select()
      .where({ 'id': pdsOtherID });

    // create assignment entry
    const dateNow = (new Date()).toISOString().slice(0, 19).replace('T', ' ');
    const assignment = await PDSAssignments
      .query()
      .insertAndFetch({
        pds_unassigned_id: pdsOtherID,
        time_assigned: dateNow,
        is_error: isError,
        pds_attribute_id: pdsItemID
      });

    // update PDS other as is_assigned = true
    const updatedPDSOther = await PDSOthers
      .query()
      .patchAndFetchById(pdsOtherID, { is_assigned: 1 });


    return [ assignment ];
  }

  async function addPDSOther(pdsOtherID, itemText, isError) {
    const { PDSOthers, PDSItems, PDSGroups, PDSAssignments } = models;

    // pull pds other so we can get its pds group ID
    const pdsOther = await PDSOthers
      .query()
      .select()
      .where({ 'id': pdsOtherID });
    const { pds_group_id } = pdsOther[0];

    console.log('got pds other item', pdsOther);
    console.log('pds_group_id', pds_group_id);

    const pdsItem = await PDSItems
      .query()
      .insertAndFetch({
        group_id: pds_group_id,
        attribute_text: itemText,
        /* @NOTE not using the name field, so using it as a comment field */
        name: 'from_pds_other_assignment'
      });

    console.log('got pds item', pdsItem);

    const pdsItemID = pdsItem['id'];
    console.log('pds item id', pdsItemID);

    // create assignment entry
    const dateNow = (new Date()).toISOString().slice(0, 19).replace('T', ' ');
    const assignment = await PDSAssignments
      .query()
      .insertAndFetch({
        pds_unassigned_id: pdsOtherID,
        time_assigned: dateNow,
        is_error: isError,
        pds_attribute_id: pdsItemID
      });

    console.log('assignment', assignment);

    // update PDS other as is_assigned = true
    const updatedPDSOther = await PDSOthers
      .query()
      .patchAndFetchById(pdsOtherID, { is_assigned: 1 });

    console.log('updatedPDSOther', updatedPDSOther);

    return [assignment, pdsItem];
  }

  return {
    getQuestions,
    getPDS,
    addNewPDS,
    updatePDS,
    deletePDS,
    addPDSItem,
    updatePDSItem,
    deletePDSItem,
    getPDSGroupUnassigned,
    getAllUnassigned,
    addPDSOther,
    assignEntry
  };
}
