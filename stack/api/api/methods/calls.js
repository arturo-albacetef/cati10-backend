module.exports = function(models) {

  async function getCalls() {

    const { Calls } = models.Calls;

    const calls = await Calls
      .query()
      .select()
      .eager('[jobs]');

    return calls;
  }

  async function getCallsTimePeriod(start, end, jobno) {
    /*
      @NOTE
        Have noticed date fields for c10_calls being non-uniform - will use the
        date_start field instead
    */

    // are start and end valid dates?


    const { Calls } = models.Calls;
    const { Jobs } = models;



    const callsPromise = Calls
      .query()
      .select()
      .where('date_start', '>=', start)
      .where('date_start', '<', end);

    if ( typeof jobno === 'string' ) {
      console.log('jobno', jobno);
      const job = await Jobs
        .query()
        .select('id')
        .where({'jobno': jobno});

      console.log('job', job);
      if ( Array.isArray(job) && job.length > 0 ) {
        const jobID = job[0]['id'];
        console.log('jobID', jobID);
        callsPromise.where({'job_id': jobID});
      }
    }

    return await callsPromise;
  }



  return { getCalls, getCallsTimePeriod }
}
