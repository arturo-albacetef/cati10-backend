module.exports = function(models) {

  return { generateTimesheets };

  async function generateTimesheets() {
    /*
      @name generateTimesheets
      @desc produces payload needed by SPA client's timesheets screen

      @TODO take in filtering parameters
    */

    // get a reference to the needed model
    const { AgentActivities } = models;

    const recordedActivities = await AgentActivities
      .query()
      .select(
        'c10_agent_activities.id as agent_activity_id',
        'c10_agent_activities.start_time',
        'c10_agent_activities.end_time',
        'c10_agent_activities.activity_id',
        'c10_agent_activities.records_viewed',
        'ag.id as agent_id',
        'ag.name as agent_name',
        'ac.codename as activity_codename',
        'ac.activity as activity_name',
        'jb.jobno as jobno',
        AgentActivities
          .relatedQuery('complete').count().as('num_completes')
      )
      .joinRelation('agent', {alias: 'ag'})
      .joinRelation('relatedActivity', {alias: 'ac'})
      .joinRelation('job', {alias: 'jb'})
      .leftJoinRelation('complete', {alias: 'c'})

    const payload = recordedActivities.map( item => {

      let hours = "N/A";
      if (item['end_time'] !== null) {
        hours = (
          item['end_time'].valueOf() - item['start_time'].valueOf()
        ) / parseFloat( 1000 * 60 * 60);
      }

      return {
        id:             item['agent_activity_id'],
        agentName:      item['agent_name'],
        start:       item['start_time'].toISOString(),
        end:         item['end_time'] === null ? null : item['end_time'].toISOString,
        hours:          hours,
        records:        parseInt(item['records_viewed'], 10),
        completes:      parseInt(item['num_completes'], 10),
        jobno:          item['jobno'],
        agent:          item['agent_name'],
        agent_id:       item['agent_id'],
        activity:       item['activity_name'],
        activity_id:    item['activity_id'],
        call_rate: (
          (typeof hours === 'number' && hours > 0)
            ? parseFloat(item['records_viewed'] / hours).toFixed(2)
            : "N/A"
        ),
        startTimestamp: item['start_time'].valueOf(),
        endTimestamp:   item['end_time'] === null ? null : item['end_time'].valueOf()
      };
    });

    return payload;
  }

}
