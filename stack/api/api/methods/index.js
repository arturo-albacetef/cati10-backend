
const initAgents      = require('./agents');
const initActivities  = require('./activities');
const initJobs        = require('./jobs');
const initTimesheets  = require('./timesheets');
const questions       = require('./questions');
const jobRules        = require('./job-rules');
const calls           = require('./calls');


module.exports = function(models) {
  return {
    activities  : initActivities(models),
    timesheets  : initTimesheets(models),
    agents      : initAgents(models),
    jobs        : initJobs(models),
    questions   : questions(models),
    jobRules    : jobRules(models),
    calls       : calls(models)
  };
}
