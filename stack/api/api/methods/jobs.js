module.exports = function(models) {
  async function getJobs() {
    const { Jobs } = models;

    const jobs = await Jobs
      .query()
      .eager('[bonusCode, state]');

    return jobs;
  }
  async function getJobsbyJobno(jobnos) {
    const { Jobs } = models;

    if( !Array.isArray(jobnos) ) {
      throw new Error(`jobnos must be array not ${typeof jobnos}`);
    }

    const jobs = await Jobs
      .query()
      .where('jobno', 'in', jobnos);

    return jobs;
  }


  async function getJobStates() {
    const { JobState } = models;
    const jobStates = await JobState.query();

    return jobStates;
  }

  async function updateJob(jobID, jobData) {
    const { Jobs } = models;

    const job = await Jobs
      .query()
      .patchAndFetchById(jobID, jobData);

    return job;
  }


  return { getJobs, getJobsbyJobno, getJobStates, updateJob };
}
