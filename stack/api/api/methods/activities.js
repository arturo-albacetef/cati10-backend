module.exports = function(models) {
  return { getActivities };

  async function getActivities() {

    const { Activities } = models;

    const data = await Activities.query();

    return data;
  }

}
