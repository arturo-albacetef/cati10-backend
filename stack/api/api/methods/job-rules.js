module.exports = function(models) {
  async function getAllowedFields() {
    const { AllowedFields } = models.JobRules;
    console.log('models.JobRules', models.JobRules);
    console.log('allowed fields', AllowedFields);
    const allowedFields = await AllowedFields
      .query();

    return allowedFields;
  }
  function LUT(op, f, val) {
    const operators = {
      '>': () => `${f} > "${val}"`,
      '>=': () => `${f} >= "${val}"`,
      '<': () => `${f} < "${val}"`,
      '<=': () => `${f} <= "${val}"`,
      '=': () => `${f} = "${val}"`,
      '!=': () => `${f} != "${val}"`,
      'contains': () => `${f} LIKE "%${val}%"`,
      'does not contain': () => `${f} IS NOT LIKE "%${val}%"`,
      'is empty': () => `${f} = "" OR ${f} IS NULL`,
      'is not empty': () => `${f} != "" AND ${f} != NULL`,
      'begins with': () => `${f} LIKE "${val}%"`,
      'ends with': () => `${f} LIKE "%${val}"`
    };

    if ( !(op in operators) ) {
      throw new Error('unknown operator:', op);
    }

    return operators[op]();
  }

  function parseObj(obj) {
    const type = obj['type'].split('query-builder-')[1];

    if ( type === 'rule' ) {
      return parseRule(obj['query']);
    } else if ( type === 'group' ) {
      return parseGroup(obj['query']);
    }

    throw new Error('Unknown type: ', type)
  }
  function parseGroup(groupObj) {
    // any -> OR , all -> AND
    const joiner = (groupObj['logicalOperator'] === 'Any')
      ? 'OR'
      : 'AND';

    const innerStr = groupObj['children']
      .map( parseObj )
      .join(  `) ${joiner} (`  );

    return `( ${innerStr} )`;
  }

  function parseRule(ruleObj) {
    const field = ruleObj['selectedOperand'];
    const value = ruleObj['value'];
    const operator = ruleObj['selectedOperator'];

    return LUT(operator, field, value);
  }

  async function previewJobRule(jobRule) {
    const { Contacts } = models;


    const clause = parseObj(jobRule);
    console.log('clause', clause);

    const data = await Contacts
      .query()
      .select()
      .whereRaw(clause);

    return data;

  }

  return { getAllowedFields, previewJobRule };
}
