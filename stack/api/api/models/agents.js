module.exports = function(Model, { Jobs } ){
  class AgentRoles extends Model {
    static get tableName() { return 'c10_agent_roles'; }
    static get jsonSchema() {
      return {
        type: 'object',

        properties: {
          id              : { type: 'integer' },
          role_shortname  : { type: 'string' },
          role_name       : { type: 'string' }
        }
      }
    }
  }
  class Agents extends Model {
    static get tableName() { return 'c10_login'; }

    static get jsonSchema() {
      return {
        type: 'object',

        properties: {
          id            : { type: 'integer' },
          name          : { type: 'string' },
          pass          : { type: 'string' },
          agent_role_id : { type: 'integer' },
          extension     : { type: ['null', 'string'] }
        }
      };
    }

    static get relationMappings() {
      return {
        role: {
          relation    : Model.HasOneRelation,
          modelClass  : AgentRoles,

          join: {
            from  : 'c10_login.agent_role_id',
            to    : 'c10_agent_roles.id'
          }
        },

        jobs: {
          relation    : Model.ManyToManyRelation,
          modelClass  : Jobs,

          join: {
            from: 'c10_login.id',
            through: {
              from  : 'c10_assigned_jobs.agent_id',
              to    : 'c10_assigned_jobs.job_id'
            },
            to: 'c10_jobs.id'
          }
        }
      }
    }
  }

  return { Agents, AgentRoles };
}
