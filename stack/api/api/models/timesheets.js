module.exports = function(Model){
  class Timesheets extends Model {
    static get tableName() { return 'c10_timesheets'; }

    static get jsonSchema() {
      return {
        type: 'object',

        properties: {
          id:           { type: 'integer' },
          start:        { type: 'string' },
          end:          { type: 'string' },
          agent_id:     { type: 'integer' },
          job_id:       { type: 'integer' },
          activity_id:  { type: 'integer' },
          hours:        { type: 'float' },
          completes:    { type: 'integer' }
        }
      };
    }
  }
  return Timesheets;
}
