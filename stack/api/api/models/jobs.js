
module.exports = function(Model) {

  class BonusCodes extends Model {
    static get tableName() { return 'c10_bonus_codes'; }
    static get jsonSchema() {
      return {
        type: 'object',

        properties: {
          id          : { type: 'integer' },
          name        : { type: 'string' },
          minrate     : { type: 'float' },
          maxrate     : { type: 'float' },
          multiplier  : { type: 'float' },
          offset      : { type: 'float' }
        }
      };
    }
  }

  class JobState extends Model {
    static get tableName() { return 'c10_job_states'; }
    static get jsonSchema() {
      return {
        type: 'object',

        properties: {
          id    : { type: 'integer' },
          name  : { type: 'string' }
        }
      }
    }
  }

  class Jobs extends Model {
    static get tableName() { return 'c10_jobs'; }

    static get jsonSchema() {
      return {
        type: 'object',

        properties: {
          id                    : { type: 'integer' },
          bonus_code_id         : { type: ['integer', 'null'] },
          job_state_id          : { type: 'integer' },
          breaking_news         : { type: ['string', 'null'] },
          jobno                 : { type: 'string' },
          description           : { type: 'string' },
          pds_group_id          : { type: ['integer', 'null'] },
        }
      };
    }
    static get relationMappings() {
      return {
        bonusCode: {
          modelClass: BonusCodes,
          relation: Model.HasOneRelation,
          join: {
            from  : 'c10_jobs.bonus_code_id',
            to    :   'c10_bonus_codes.id'
          }
        },
        state: {
          modelClass: JobState,
          relation: Model.HasOneRelation,
          join: {
            from  : 'c10_jobs.job_state_id',
            to    : 'c10_job_states.id'
          }
        }
      }
    }
  }

  return { BonusCodes, Jobs, JobState };
}
