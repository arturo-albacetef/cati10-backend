module.exports = function(Model, { Jobs, Agents }) {

  class Activities extends Model {
    static get tableName() { return 'c10_activities'; }
    static get jsonSchema() {
      return {
        type: 'object',
        properties: {
          id:       { type: 'integer' },
          activity: { type: 'string' },
          codename: { type: 'string' }
        }
      }
    }
  }

  class EventTypes extends Model {
    static get tableName() { return 'c10_event_types' }
    static get jsonSchema() {
      return {
        type: 'object',

        properties: {
          id:       { type: 'integer' },
          name:     { type: 'string' },
          codename: { type: 'string' }
        }
      }
    }
  }

  class AgentEvents extends Model {
    static get tableName() { return 'c10_agent_events'; }
    static get jsonSchema() {
      return {
        type: 'object',
        properties: {
          id:             { type: 'integer' },
          agent_id:       { type: 'integer' },
          event_type_id:  { type: 'integer' },
          activity_id:    { type: 'integer' },
          date_updated:   { type: 'datetime' },
          call_id:        { type: 'integer' }
        }
      }
    }
  }

  class AgentActivities extends Model {
    static get tableName() { return 'c10_agent_activities'; }
    static get jsonSchema() {
      return {
        type: 'object',
        properties: {
          id:             { type: 'integer' },
          agent_id:       { type: 'integer' },
          event_type_id:  { type: 'integer' },
          activity_id:    { type: 'integer' },
          agent_event_id: { type: 'integer' },
          job_id:         { type: 'integer' },
          start_time:     { type: 'datetime' },
          end_time:       { type: 'datetime' },
          records_viewed: { type: 'integer' }
        }
      }
    }
    static get relationMappings() {
      return {
        relatedActivity: {
          relation: Model.BelongsToOneRelation,
          modelClass: Activities,
          join: {
            from: 'c10_agent_activities.activity_id',
            to: 'c10_activities.id'
          }
        },

        startEvent: {
          relation: Model.BelongsToOneRelation,
          modelClass: AgentEvents,
          join: {
            from: 'c10_agent_activities.agent_event_id',
            to: 'c10_agent_events.id'
          }
        },

        eventType: {
          relation: Model.BelongsToOneRelation,
          modelClass: EventTypes,
          join: {
            from: 'c10_agent_activities.event_type_id',
            to: 'c10_event_types.id'
          }
        },

        job: {
          relation: Model.BelongsToOneRelation,
          modelClass: Jobs,
          join: {
            from: 'c10_agent_activities.job_id',
            to: 'c10_jobs.id'
          }
        },

        agent: {
          relation: Model.BelongsToOneRelation,
          modelClass: Agents,
          join: {
            from: 'c10_agent_activities.agent_id',
            to: 'c10_login.id'
          }
        },

        complete: {
          relation: Model.BelongsToOneRelation,
          modelClass: ActivityCompletes,
          join: {
            from: 'c10_activity_completes.agent_activity_id',
            to: 'c10_agent_activities.id'
          }
        }
      }
    }
  }

  class ActivityCompletes extends Model {
    static get tableName() { return 'c10_activity_completes'; }
    static get jsonSchema() {
      return {
        type: 'object',
        properties: {
          id:                 { type: 'integer' },
          agent_activity_id:  { type: 'integer' },
          contact_id:         { type: 'integer' }
        }
      }
    }
  }

  return {
    Activities,
    EventTypes,
    AgentEvents,
    AgentActivities,
    ActivityCompletes
  };
}
