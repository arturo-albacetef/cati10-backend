module.exports = function(Model) {
  class Contacts extends Model {
    static get tableName() { return 'c10_contacts'; }

    static get jsonSchema() {
      return {
        type: 'object',

        /* @TODO finish adding props */
        properties: {
          extlink       : { type: 'integer' },
          idnum         : { type: 'string' },
          cname         : { type: 'string' },
          surname       : { type: 'string' }
        }
      };
    }
  }

  return { Contacts };
}
