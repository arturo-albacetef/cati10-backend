module.exports = function(Model, { Jobs }) {


  class Calls extends Model {
    static get tableName() { return 'c10_calls'; }
    static get jsonSchema() {
      return {
        type: 'object',

        properties: {
          id                  : { type: 'integer' },
          job_id              : { type: 'integer' },
          contact_id          : { type: 'integer' },
          call_state_id       : { type: 'integer' },

          date_start          : { type: 'datetime' },
          date_end            : { type: 'datetime' },
          last_updated_date   : { type: 'datetime' },
          call_state_updated  : { type: 'datetime' },

          comment             : { type: 'string' },
          completed           : { type: 'integer' },
          objection           : { type: 'string' }
        }
      }
    }

    static get relationMappings() {
      return {
        job: {
          relation    : Model.HasOneRelation,
          modelClass  : Jobs,

          join: {
            from: 'c10_calls.job_id',
            to  : 'c10_jobs.id'
          }
        }
      }
    }
  }


  return { Calls };
}
