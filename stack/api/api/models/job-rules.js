module.exports = function(Model) {

  class AllowedFields extends Model {
    static get tableName() { return 'c10_job_rules_allowed_fields'; }
  }

  return { AllowedFields };
}
