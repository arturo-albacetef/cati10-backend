

module.exports = (function() {

  /* @TODO move to config file */
  const knex = require('knex')({
    client: "mysql",
    connection: {
      host: "db-host",
      port: "3306",
      user: "root",
      password: "example",
      database: "ukdatab"
    }
  });

  const { Model, transaction } = require('objection');
  Model.knex(knex);

  const { BonusCodes, Jobs, JobState } = require('./jobs')(Model);
  const { Agents, AgentRoles } = require('./agents')(Model, { Jobs });
  const {
    Activities,
    EventTypes,
    AgentEvents,
    AgentActivities,
    ActivityCompletes
  } = require('./activities')(Model, { Jobs, Agents });

  const {
    Questions,
    PDSGroups,
    PDSItems,
    PDSOthers,
    PDSAssignments
  } = require('./questions')(Model, { Jobs });

  const { AllowedFields } = require('./job-rules')(Model);
  const Calls = require('./calls')(Model, { Jobs });
  const { Contacts } = require('./contacts')(Model);

  return {
    transaction,
    Activities        : Activities,
    ActivityCompletes : ActivityCompletes,
    AgentActivities   : AgentActivities,
    AgentEvents       : AgentEvents,
    Agents            : Agents,
    AgentRoles        : AgentRoles,
    BonusCodes        : BonusCodes,
    Calls             : Calls,
    Contacts          : Contacts,
    EventTypes        : EventTypes,
    Jobs              : Jobs,
    JobState          : JobState,
    Questions         : Questions,
    PDSGroups         : PDSGroups,
    PDSItems          : PDSItems,
    PDSOthers         : PDSOthers,
    PDSAssignments    : PDSAssignments,
    Timesheets        : require('./timesheets')(Model),
    JobRules          : { AllowedFields }
  };
})();
