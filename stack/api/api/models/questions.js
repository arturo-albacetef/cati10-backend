module.exports = function(Model, { Jobs }) {
  class QuestionTypes extends Model {
    static get tableName() { return 'c10_question_types'; }
  }

  class Questions extends Model {
    static get tableName() { return 'c10_questions'; }
    static get relationMappings() {
      return {
        job: {
          modelClass: Jobs,
          relation: Model.HasOneRelation,
          join: {
            from: 'c10_questions.job_id',
            to: 'c10_jobs.id'
          }
        },
        questionType: {
          modelClass: QuestionTypes,
          relation: Model.HasOneRelation,
          join: {
            from: 'c10_questions.type_id',
            to  : 'c10_question_types.id'
          }
        }
      };
    }
  }

  class PDSItems extends Model {
    static get tableName() { return 'c10_pds_attributes'; }
    static get jsonSchema() {
      return {
        type: 'object',

        properties: {
          id                : { type: 'integer' },
          group_id          : { type: 'integer' },
          name              : { type: 'string ' },
          attribute_text    : { type: 'string' },
          is_active         : { type: 'integer' },
          seq               : { type: 'integer' }
        }
      }
    }
  }

  class PDSGroups extends Model {
    static get tableName() { return 'c10_pds_groups' }
    static get jsonSchema() {
      return {
        type: 'object',

        properties: {
          id          : { type: 'integer' },
          name        : { type: 'string' },
          comment     : { type: ['string', 'null'] },
          is_active   : { type: 'integer' },
          text        : { type: 'string' },
          job_id      : { type: 'integer' }
        }
      }
    }
    static get relationMappings() {
      return {
        others: {
          modelClass: PDSOthers,
          relation: Model.HasManyRelation,
          join: {
            from: 'c10_pds_groups.id',
            to  : 'c10_pds_unassigned.pds_group_id'
          }
        },

        items: {
          modelClass: PDSItems,
          relation: Model.HasManyRelation,
          join: {
            from: 'c10_pds_groups.id',
            to  : 'c10_pds_attributes.group_id'
          }
        },

        job: {
          modelClass: Jobs,
          relation: Model.HasOneRelation,
          join: {
            from: 'c10_pds_groups.job_id',
            to: 'c10_jobs.id'
          }
        }
      }
    }
  }

  class PDSOthers extends Model {
    static get tableName() { return 'c10_pds_unassigned'; }
    static get jsonSchema() {
      return {
        type: 'object',

        properties: {
          id              : { type: 'integer' },
          pds_group_id    : { type: 'integer' },
          call_id         : { type: 'integer' },
          date_inserted   : { type: 'date' },
          item_text       : { type: 'string' },
          is_assigned     : { type: 'integer' }
        }
      }
    }
  }

  class PDSAssignments extends Model {
    static get tableName() { return 'c10_pds_assignments'; }
    static get jsonSchema() {
      return {
        type: 'object',

        properties: {
          id                  : { type: 'integer' },
          pds_unassigned_id   : { type: 'integer' },
          time_assigned       : { type: 'datetime' },
          agent_id            : { type: ['null', 'integer'] },
          is_error            : { type: ['null', 'integer'] },
          pds_attribute_id    : { type: 'integer' }
        }
      }
    }
  }


  return { Questions, PDSGroups, PDSItems, PDSOthers, PDSAssignments };
}
