const Hapi = require('hapi');
const path = require('path');

const { requestLogging, serverLogging } = require('./logging');

let server = Hapi.server({
  port: 3001,
  host: 'api-host'
});


process.on('unhandledRejection', err => {
  console.log('Unhandled failure: ', err);
  process.exit(1);
})

async function init() {
  await server.start({
    routes: {
      files: {
        relativeTo: path.resolve('/var/www/cati10-frontend/dist')
      }
    }
  });

  // set up log handlers
  server.events.on('request', requestLogging);
  server.events.on('log', serverLogging);


  // static file serving plugin
  await server.register(require('inert'));

  // load models
  const models = require('./models');
  console.log('initialized models');

  // initialize methods module
  const methods = require('./methods')(models);


  // register routes
  const initRoutes = require('./routes');
  initRoutes(server, methods);
  console.log('routes registered');


  server.log(['initialization', 'debug'], 'init server', (Date.now() - 100) )
  console.log(`Server running at: ${server.info.uri}`);
}

init();
