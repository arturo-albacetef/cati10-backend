const mockPayload = {
  "data": {
    "headers": [
      { "text": "Idnum",      "value": "idnum",     "align": "left" },
      { "text": "Datestamp",  "value": "datestamp", "align": "left" },
      { "text": "Activity",   "value": "activity",  "align": "left" }
    ],
    "rows": [
      {
        "idnum": "YP75606",
        "datestamp": "25/10/2018 08:51:10",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP75606",
        "datestamp": "25/10/2018 08:58:04",
        "activity": "Started"
      },
      {
        "idnum": "YP75606",
        "datestamp": "25/10/2018 09:27:57",
        "activity": "Complete 1"
      },
      {
        "idnum": "YP75606",
        "datestamp": "25/10/2018 09:27:57",
        "activity": "Ended"
      },
      {
        "idnum": "YP6011",
        "datestamp": "25/10/2018 09:28:54",
        "activity": "Auto-served"
      },
      {
        "idnum": "MSD11392",
        "datestamp": "25/10/2018 09:30:18",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP82691",
        "datestamp": "25/10/2018 09:31:00",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q330316",
        "datestamp": "25/10/2018 09:31:55",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q221675",
        "datestamp": "25/10/2018 09:32:35",
        "activity": "Auto-served"
      },
      {
        "idnum": "15868",
        "datestamp": "25/10/2018 09:33:21",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP73313",
        "datestamp": "25/10/2018 09:34:04",
        "activity": "Auto-served"
      },
      {
        "idnum": "67135",
        "datestamp": "25/10/2018 09:37:17",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q620512",
        "datestamp": "25/10/2018 09:38:39",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q307616",
        "datestamp": "25/10/2018 09:39:14",
        "activity": "Auto-served"
      },
      {
        "idnum": "MSD 2998",
        "datestamp": "25/10/2018 09:39:49",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP55963",
        "datestamp": "25/10/2018 09:40:27",
        "activity": "Auto-served"
      },
      {
        "idnum": "45056",
        "datestamp": "25/10/2018 09:40:58",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP9956",
        "datestamp": "25/10/2018 09:41:38",
        "activity": "Auto-served"
      },
      {
        "idnum": "71956",
        "datestamp": "25/10/2018 09:42:11",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP40705",
        "datestamp": "25/10/2018 09:42:59",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP13512",
        "datestamp": "25/10/2018 09:44:09",
        "activity": "Auto-served"
      },
      {
        "idnum": "JK25234",
        "datestamp": "25/10/2018 09:45:15",
        "activity": "Auto-served"
      },
      {
        "idnum": "MSD10120",
        "datestamp": "25/10/2018 09:46:35",
        "activity": "Auto-served"
      },
      {
        "idnum": "55587",
        "datestamp": "25/10/2018 09:47:39",
        "activity": "Auto-served"
      },
      {
        "idnum": "69264",
        "datestamp": "25/10/2018 09:48:17",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP15538",
        "datestamp": "25/10/2018 09:49:38",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q330555",
        "datestamp": "25/10/2018 09:50:17",
        "activity": "Auto-served"
      },
      {
        "idnum": "67621",
        "datestamp": "25/10/2018 09:50:31",
        "activity": "Auto-served"
      },
      {
        "idnum": "44598",
        "datestamp": "25/10/2018 09:51:16",
        "activity": "Auto-served"
      },
      {
        "idnum": "64973",
        "datestamp": "25/10/2018 09:51:49",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP19498",
        "datestamp": "25/10/2018 09:52:26",
        "activity": "Auto-served"
      },
      {
        "idnum": "1265SEM1",
        "datestamp": "25/10/2018 09:53:05",
        "activity": "Auto-served"
      },
      {
        "idnum": "JK23939",
        "datestamp": "25/10/2018 09:54:45",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP80859",
        "datestamp": "25/10/2018 09:56:00",
        "activity": "Auto-served"
      },
      {
        "idnum": "47076",
        "datestamp": "25/10/2018 09:57:08",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q316929",
        "datestamp": "25/10/2018 09:58:05",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q1509",
        "datestamp": "25/10/2018 09:59:02",
        "activity": "Auto-served"
      },
      {
        "idnum": "44627",
        "datestamp": "25/10/2018 09:59:51",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q223896",
        "datestamp": "25/10/2018 10:00:24",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP63918",
        "datestamp": "25/10/2018 10:06:48",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP19800",
        "datestamp": "25/10/2018 10:07:23",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP  917",
        "datestamp": "25/10/2018 10:07:51",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP82256",
        "datestamp": "25/10/2018 10:08:36",
        "activity": "Auto-served"
      },
      {
        "idnum": "NI20372",
        "datestamp": "25/10/2018 10:09:25",
        "activity": "Auto-served"
      },
      {
        "idnum": "55659",
        "datestamp": "25/10/2018 10:10:02",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q650",
        "datestamp": "25/10/2018 10:16:25",
        "activity": "Picked"
      },
      {
        "idnum": "YP20982",
        "datestamp": "25/10/2018 10:19:37",
        "activity": "Auto-served"
      },
      {
        "idnum": "11801",
        "datestamp": "25/10/2018 10:20:16",
        "activity": "Auto-served"
      },
      {
        "idnum": "66483",
        "datestamp": "25/10/2018 10:20:50",
        "activity": "Auto-served"
      },
      {
        "idnum": "49222",
        "datestamp": "25/10/2018 10:21:41",
        "activity": "Auto-served"
      },
      {
        "idnum": "02817",
        "datestamp": "25/10/2018 10:22:45",
        "activity": "Auto-served"
      },
      {
        "idnum": "MSD 3301",
        "datestamp": "25/10/2018 10:24:21",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP14104",
        "datestamp": "25/10/2018 10:25:14",
        "activity": "Auto-served"
      },
      {
        "idnum": "24620",
        "datestamp": "25/10/2018 10:26:03",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q321707",
        "datestamp": "25/10/2018 10:26:52",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q318547",
        "datestamp": "25/10/2018 10:27:30",
        "activity": "Auto-served"
      },
      {
        "idnum": "30221",
        "datestamp": "25/10/2018 10:49:41",
        "activity": "Auto-served"
      },
      {
        "idnum": "16823",
        "datestamp": "25/10/2018 10:50:34",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP34449",
        "datestamp": "25/10/2018 10:51:10",
        "activity": "Auto-served"
      },
      {
        "idnum": "67256",
        "datestamp": "25/10/2018 10:51:54",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q313309",
        "datestamp": "25/10/2018 10:52:47",
        "activity": "Auto-served"
      },
      {
        "idnum": "14072",
        "datestamp": "25/10/2018 10:53:35",
        "activity": "Auto-served"
      },
      {
        "idnum": "67941",
        "datestamp": "25/10/2018 10:54:24",
        "activity": "Auto-served"
      },
      {
        "idnum": "BYP41005",
        "datestamp": "25/10/2018 10:55:24",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q1018448",
        "datestamp": "25/10/2018 10:56:14",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP19865",
        "datestamp": "25/10/2018 10:56:53",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q22204",
        "datestamp": "25/10/2018 10:57:35",
        "activity": "Auto-served"
      },
      {
        "idnum": "00652",
        "datestamp": "25/10/2018 10:58:31",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP55474",
        "datestamp": "25/10/2018 10:59:48",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q521550",
        "datestamp": "25/10/2018 11:00:46",
        "activity": "Picked"
      },
      {
        "idnum": "68711",
        "datestamp": "25/10/2018 11:03:00",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q1887",
        "datestamp": "25/10/2018 11:03:23",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP16012",
        "datestamp": "25/10/2018 11:04:58",
        "activity": "Auto-served"
      },
      {
        "idnum": "64352",
        "datestamp": "25/10/2018 11:05:33",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP41806",
        "datestamp": "25/10/2018 11:07:33",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q3728",
        "datestamp": "25/10/2018 11:08:16",
        "activity": "Auto-served"
      },
      {
        "idnum": "MSD 4772",
        "datestamp": "25/10/2018 11:08:58",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP7017",
        "datestamp": "25/10/2018 11:09:43",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP71967",
        "datestamp": "25/10/2018 11:10:29",
        "activity": "Auto-served"
      },
      {
        "idnum": "RD199",
        "datestamp": "25/10/2018 11:10:56",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP17263",
        "datestamp": "25/10/2018 11:11:46",
        "activity": "Auto-served"
      },
      {
        "idnum": "16491",
        "datestamp": "25/10/2018 11:15:33",
        "activity": "Auto-served"
      },
      {
        "idnum": "70158",
        "datestamp": "25/10/2018 11:16:25",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q178968",
        "datestamp": "25/10/2018 11:17:08",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q178898",
        "datestamp": "25/10/2018 11:17:22",
        "activity": "Auto-served"
      },
      {
        "idnum": "54763",
        "datestamp": "25/10/2018 11:19:03",
        "activity": "Auto-served"
      },
      {
        "idnum": "04255",
        "datestamp": "25/10/2018 11:19:49",
        "activity": "Auto-served"
      },
      {
        "idnum": "KTH19861",
        "datestamp": "25/10/2018 11:20:56",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP12948",
        "datestamp": "25/10/2018 11:21:40",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q4302",
        "datestamp": "25/10/2018 11:22:21",
        "activity": "Auto-served"
      },
      {
        "idnum": "789P",
        "datestamp": "25/10/2018 11:23:21",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP6149",
        "datestamp": "25/10/2018 11:23:48",
        "activity": "Auto-served"
      },
      {
        "idnum": "21952",
        "datestamp": "25/10/2018 11:24:38",
        "activity": "Auto-served"
      },
      {
        "idnum": "JK26927",
        "datestamp": "25/10/2018 11:25:17",
        "activity": "Auto-served"
      },
      {
        "idnum": "22011",
        "datestamp": "25/10/2018 11:26:10",
        "activity": "Auto-served"
      },
      {
        "idnum": "54783",
        "datestamp": "25/10/2018 11:27:24",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP67833",
        "datestamp": "25/10/2018 11:29:30",
        "activity": "Auto-served"
      },
      {
        "idnum": "31344",
        "datestamp": "25/10/2018 11:30:20",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q13023",
        "datestamp": "25/10/2018 11:31:12",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q335359",
        "datestamp": "25/10/2018 11:32:32",
        "activity": "Picked"
      },
      {
        "idnum": "27295",
        "datestamp": "25/10/2018 11:34:54",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q1018115",
        "datestamp": "25/10/2018 11:35:57",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP73197",
        "datestamp": "25/10/2018 11:36:57",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q625048",
        "datestamp": "25/10/2018 11:39:12",
        "activity": "Auto-served"
      },
      {
        "idnum": "MSD 6176",
        "datestamp": "25/10/2018 11:39:51",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP43431",
        "datestamp": "25/10/2018 11:40:32",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP75433",
        "datestamp": "25/10/2018 11:40:56",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q5906",
        "datestamp": "25/10/2018 11:41:34",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q189712",
        "datestamp": "25/10/2018 11:42:11",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP82679",
        "datestamp": "25/10/2018 11:44:10",
        "activity": "Auto-served"
      },
      {
        "idnum": "IRL3349",
        "datestamp": "25/10/2018 11:44:54",
        "activity": "Auto-served"
      },
      {
        "idnum": "15765",
        "datestamp": "25/10/2018 11:45:39",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP3771",
        "datestamp": "25/10/2018 11:46:24",
        "activity": "Auto-served"
      },
      {
        "idnum": "47720",
        "datestamp": "25/10/2018 11:47:32",
        "activity": "Auto-served"
      },
      {
        "idnum": "72278",
        "datestamp": "25/10/2018 11:48:35",
        "activity": "Auto-served"
      },
      {
        "idnum": "53957",
        "datestamp": "25/10/2018 11:49:08",
        "activity": "Auto-served"
      },
      {
        "idnum": "32401",
        "datestamp": "25/10/2018 11:50:01",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q2783",
        "datestamp": "25/10/2018 11:50:38",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q5919",
        "datestamp": "25/10/2018 11:51:15",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP32656",
        "datestamp": "25/10/2018 11:51:51",
        "activity": "Auto-served"
      },
      {
        "idnum": "Q187500",
        "datestamp": "25/10/2018 11:52:14",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP54362",
        "datestamp": "25/10/2018 11:52:54",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP22051",
        "datestamp": "25/10/2018 11:53:20",
        "activity": "Auto-served"
      },
      {
        "idnum": "61392",
        "datestamp": "25/10/2018 11:55:20",
        "activity": "Auto-served"
      },
      {
        "idnum": "YP21401",
        "datestamp": "25/10/2018 11:56:15",
        "activity": "Auto-served"
      }
    ]

  },
  "errors": null
};


module.exports = (function(){
  const idnum = {
    method: 'GET',
    path:   '/idnum',
    options: { log: { collect: true }},

    handler(req, h) {
      return h.response(mockPayload);
    }
  }

  return idnum
})();
