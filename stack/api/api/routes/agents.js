module.exports = function(methods) {
  return [
    {
      method: 'GET',
      path: '/agents',
      options: { log: { collect: true }},

      handler: async function(req, h) {

        const { getAgents } = methods.agents;

        let agents = null;
        try {
          agents = await getAgents();
        } catch(err) {
          console.error(err);
          return h.response({ data: null, errors: [ err ] }).code(500);
        }

        return h.response({ data: agents, errors: null });
      }
    },
    {
      method: 'POST',
      path: '/agents',
      options: { log: { collect: true }},

      handler: async function(req, h) {
        const { addNewAgent } = methods.agents;
        let data = null;
        let errors = null;
        let statusCode = 500;

        try {
          const { name, password, role, extension } = req.payload;
          data = await addNewAgent(
            name,
            password,
            role,
            extension
          )
          statusCode = 201;
        } catch(err) {
          console.log('got error', err);
          errors = [ err ];
        }

        return h.response({ data, errors }).code(statusCode);
      }
    },
    {
      method: 'POST',
      path: '/agents/{id}',
      options: { log: { collect: true }},

      handler: async function(req, h) {

        const { updateAgent } = methods.agents;

        if( typeof req.params.id !== 'string' || req.params.id.length === 0 ) {
          return h
            .response({data: {}, errors: 'missing ID' })
            .code(400);
        }

        const id = req.params.id;
        const { name, agentRoleID, extension } = req.payload;

        if( typeof name === 'undefined' || typeof agentRoleID === 'undefined') {
          return h
            .response({data: null, errors: 'requires: [name, agentRoleID]'})
            .code(400);
        }

        try {
          const data = await updateAgent(id, name, agentRoleID, extension);
          return h.response({data, errors: null});
        } catch(err) {
          console.log('got error', err);
          return h.response({data: null, errors: [err]}).code(500);
        }

      }
    },
    {
      method: 'POST',
      path: '/agents/{id}/change-password',
      options: { log: { collect: true }},

      handler: async function(req, h) {
        const { updatePassword } = methods.agents;
        const { id } = req.params;
        let data = null;
        let errors = null;
        let code = 500;

        if (typeof id === 'undefined' || id === null ) {
          return h
            .response({data, errors: [ 'Invalid parameters']})
            .code(400);
        }

        try {
          const { currPass, newPass } = req.payload;
          data = await updatePassword(id, currPass, newPass);
          code = 200;
        } catch(err) {
          console.log('err', err);
          errors = [ err.message ];
        }

        return h.response({ data, errors });
      }
    },

    {
      method: 'POST',
      path: '/agents/{id}/unassign-jobs',
      options: { log: { collect: true }} ,

      handler: async function(req, h) {

        const { unassignJobs } = methods.agents;
        const jobIDs = req.payload.jobIDs
          .map( j => j.id );

        const id = req.params.id;

        try {
          const data = await unassignJobs(id, jobIDs);
          return h.response({data, errors: null});
        } catch(err) {
          console.log('error ocurred', err);
          return h
            .response({data: null, errors: [err]})
            .code(500);
        }

      }
    },

    {
      method: 'POST',
      path: '/agents/{id}/assign-jobs',
      options: { log: { collect: true }},

      handler: async function(req, h) {

        const { assignJobs } = methods.agents;

        const jobs = req.payload.jobs;
        const agentID = req.params.id;
        console.log( 'jobs', jobs );
        const data = await assignJobs(agentID, jobs);

        return h.response({ data, errors: null });
      }
    }
  ];
}
