module.exports = function(methods) {
  return [
    {
      method: 'GET',
      path:   '/calls',
      options: { log: { collect: true }},

      handler: async function(req, h) {

      }
    },
    {
      method:   'POST',
      path:     '/calls/in-date',
      options:  { log: { collect: true }},

      handler:  async function(req, h) {

        const { getCallsTimePeriod } = methods.calls;
        const { start, end } = req.payload;

        if ( typeof start === 'undefined' || typeof end === 'undefined' ) {
          return h.response({
            data: null,
            errors: [ 'Please provide both a start and end date']
          }).code(400);
        }

        let data = null;
        let errors = null;
        let code = 500;
        try {
          data = await getCallsTimePeriod(start, end);
          code = 200;
        } catch(err) {
          errors = [ err.message ];
        }

        return h.response({
          data,
          errors
        }).code(code);
      }
    }



  ];
}
