const dateFns = require('date-fns');

module.exports = function(methods){

  const callReport = {
    method: 'GET',
    path:   '/call-report/{jobno?}',
    options: { log: { collect: true }},

    handler: async function(req, h) {
      req.log(['api', 'report'], req.payload);

      const { getCallsTimePeriod } = methods.calls;

      let jobno = req.params.jobno || null;
      let data = null;
      let errors = null;
      let code = 500;

      try {
        const nowStamp = Date.now();
        const start = dateFns.startOfYear(nowStamp);
        const end   = (new Date()).toISOString();
        callData = await getCallsTimePeriod(start, end, jobno);

        const dates = {
          today : dateFns.startOfToday(nowStamp),
          week  : dateFns.startOfWeek(nowStamp),
          month : dateFns.startOfMonth(nowStamp),
          year  : dateFns.startOfYear(nowStamp)
        };

        const numCalls = {
          today : 0,
          week  : 0,
          month : 0,
          year  : 0
        };

        const isValid = (a, b) => dateFns.compareAsc(a, b) > -1;
        console.log('startTime', start);
        console.log('end', end);
        console.log('dates', dates);
        for( let callItem of callData ) {

          const startDate = callItem['date_start'];
          if (isValid(startDate, dates['today']) ) {
            numCalls['today'] += 1;
          }

          if (isValid(startDate, dates['week'])) {
            numCalls['week'] += 1;
          }

          if (isValid(startDate, dates['month'])) {
            numCalls['month'] += 1;
          }

          if (isValid(startDate, dates['year'])) {
            numCalls['year'] += 1;
          }
        }

        data = {
          headers: [
            { text: "Period", value: "period", align: "left" },
            { text: "# of calls", value: "num_calls" }
          ],
          rows: [
            { period: "Today", num_calls: numCalls['today'],  date: dates['today'].toISOString()},
            { period: "This week", num_calls: numCalls['week'],  date: dates['week'].toISOString()},
            { period: "This month", num_calls: numCalls['month'], date: dates['month'].toISOString() },
            { period: "This year", num_calls: numCalls['year'], date: dates['year'].toISOString() }
          ]
        };
        code = 200;
      } catch(err) {
        errors = [ err.message ];
      }

      return h.response({
        data,
        errors
      }).code(code);

    }
  };



  return callReport;
};
