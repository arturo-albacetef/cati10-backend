const path = require('path');

// custom routes
const callReport  = require('./call-report');
const agents      = require('./agents');
const idnum       = require('./idnum');
const timesheets  = require('./timesheets');
const jobs        = require('./jobs');
const activity    = require('./activity');
const api         = require('./api');
const questions   = require('./questions');
const jobRules    = require('./job-rules');
const calls       = require('./calls');

module.exports = function(server, methods) {

  const wwwPath = '/var/www/cati10-frontend';
  const indexName = 'index.html';


  // serve app entrypoint
  server.route({
    method: 'GET',
    path:   '/',
    options: { log: { collect: true }},
    handler(req, h) {
      return h.file(wwwPath +'/' + indexName, { confine: false } );
    }
  });

  // static file serving
  server.route({
    method: 'GET',
    path:   '/static/{param*}',
    handler: {
      directory: {
        path: path.join(wwwPath, 'dist', 'static')
      }
    }
  });

  // register /api endpoints
  for( let endpoint of api(methods) ) {
    server.route(endpoint);
  }
  server.route(callReport(methods));
  server.route(agents(methods));
  server.route(idnum);
  server.route(timesheets(methods));
  server.route(jobs(methods));
  server.route(activity(methods));
  server.route(questions(methods));
  server.route(jobRules(methods));
  server.route(calls(methods));

}
