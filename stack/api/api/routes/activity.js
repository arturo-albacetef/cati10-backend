module.exports = function(methods) {
  return {
    method: 'GET',
    path:   '/activity',
    options: { log: { collect: true }},

    handler: async function(req, h) {
      const activities = await methods.activities.getActivities();
      return h.response({ data: activities, errors: null });
    }
  };
}
