
module.exports = function(methods) {
  const { generateTimesheets } = methods.timesheets;

  return {
    method: 'GET',
    path:   '/timesheets',
    options: { log: { collect: true }},

    handler: async function(req, h) {
      let timesheets = null;
      try {
        timesheets = await generateTimesheets();
      } catch(err) {
        console.log('ot error', err);
        return h.response({ data: null, errors: [err] });
      }

      const payload = {
        headers: [
          { "text" : "Start", "value": "start", "align": "left" },
          { "text" : "End", "value": "end", "align": "left" },
          { "text" : "Agent", "value": "agent", "align": "left" },
          { "text" : "Job #", "value": "jobno", "align": "left" },
          { "text" : "Activity", "value": "activity", "align": "left" },
          { "text" : "Hours", "value": "hours", "align": "left" },
          { "text" : "Completes", "value": "completes", "align": "left" },
          { "text" : "Call rate", "value": "call_rate", "align": "left" },
          { "text" : "Records", "value": "records", "align": "left" }
        ],
        rows: timesheets
      };

      return h.response({ data: payload, errors: null });
    }
  }

};
