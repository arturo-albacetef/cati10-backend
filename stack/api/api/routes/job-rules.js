module.exports = function(methods) {
  return [
    {
      method: 'GET',
      path: '/job-rules-fields',
      options: { log: { collect: true } },

      handler: async function(req, h) {
        const { getAllowedFields } = methods.jobRules;

        const data = await getAllowedFields();

        return h.response({
          data,
          errors: null
        }).code(200);
      }
    },
    {
      method: 'POST',
      path: '/job-rule-preview',

      handler: async function(req, h) {

        const { previewJobRule } = methods.jobRules;
        const { queryObj, parsedQuery, jobno } = req.payload.data;

        let data = null;
        let errors = null;
        let code = 500;

        try {
          data = await previewJobRule(queryObj, jobno);
          code = 200;
        } catch(err) {
          console.log(err);
          errors = [ err.message ];
        }

        return h.response({data, errors }).code(code);
      }
    }
  ]


}
