
module.exports = function(methods) {
  return [
    {
      method: 'GET',
      path: '/api/agent-roles',
      handler: async function(req, h) {

        const { getAgentRoles } = methods.agents;

        let data = null;
        if( Array.isArray(req.params.roleIDs) ) {
          let roleIDs = req.params.roleIDs;
          data = await getAgentRoles(roleIDs);
        } else {
          data = await getAgentRoles();
        }

        return h.response({ data, errors: null }).code(200);
      }
    },
    {
      method: 'GET',
      path: '/api/jobs',
      handler: async function(req, h) {
        const { getJobs } = methods.jobs;

        const data = await getJobs();

        return h.response({data, errors: null}).code(200);
      }
    }
  ];
};
