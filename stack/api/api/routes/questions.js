module.exports = function(methods){
  const jobs = [{
    method: 'GET',
    path:   '/pds',
    options: { log: { collect: true }},

    handler: async function(req, h) {

      const { getPDS } = methods.questions;

      let pds = null;
      try {
        pds = await getPDS();
      } catch(err) {
        console.log(err);
        return h.response({data: null, errors: [ err ]}).code(500);
      }

      return h.response({
        data: pds,
        errors: null
      });
    }
  },
  {
    method: 'POST',
    path: '/pds-groups/{pdsID?}',
    options: { log: { collect: true }},

    handler: async function(req, h) {

      const { addNewPDS, updatePDS } = methods.questions;
      const payload = req.payload;

      let data = null;
      let errors = null;
      let code = 500;
      if (req.params.pdsID === '' || typeof req.params.pdsID === 'undefined') {
        // creating new PDS group
        console.log('create new pds group');
        try {
          data = await addNewPDS(payload);
          code = 201;
        } catch( err ) {
          console.log('got error adding new PDS', err);
          console.log('payload: ', payload);

          errors = [ err.message ];
        }

      } else {
        // updating PDS
        try {
          data = await updatePDS(req.params.pdsID, payload);
          code = 200;
        } catch(err) {
          console.log('got error updating PDS', err);
          console.log('payload: ', payload);
          errors = [ err.message ];
        }
      }

      return h.response({
        data,
        errors
      }).code(code);
    }
  },
  {
    method: 'DELETE',
    path: '/pds-groups/{pdsID}',
    options: { log: { collect: true }},

    handler: async function(req, h) {
      const { deletePDS } = methods.questions;

      const pdsID = req.params.pdsID;

      let data = null;
      let errors = null;
      let code = 500;
      try {
        data = await deletePDS(pdsID);
        code = 200;
      } catch(err) {
        console.log('got error deleting PDS (archiving)', err);
        console.log('id', pdsID);
        errors = [ err.message ];
      }

      return h.response({ data, errors });
    }
  },
  {
    method: 'POST',
    path: '/pds-items/{pdsItemID?}',

    handler: async function(req, h) {
      const { addPDSItem, updatePDSItem } = methods.questions;
      const payload = req.payload;

      let data = null;
      let errors = null;
      let code = 500;

      if (req.params.pdsItemID === '' || typeof req.params.pdsItemID === 'undefined') {
        // create pds item
        try {
          const { name, group_id, attribute_text } = payload.pdsItem;
          data = await addPDSItem( {name, group_id, attribute_text } )
          code = 201;
        } catch(err) {
          console.log('got error creating PDS item', err);
          errors = [ err.message ];
        }
      } else {

        try {
          const { attribute_text } = payload.pdsItem;
          data = await updatePDSItem( req.params.pdsItemID, {attribute_text } );
          code = 200;
        } catch(err) {
          console.log('got error updating PDS item', err);
          console.log('id', req.params.pdsItemID);
          errors = [ err.message ];
        }
      }

      return h.response({ data, errors }).code(code);
    }
  },
  {
    method: 'DELETE',
    path: '/pds-items/{pdsItemID}',
    options: { log: { collect: true }},

    handler: async function(req, h) {
      const { deletePDSItem } = methods.questions;

      const pdsItemID = req.params.pdsItemID;

      let data = null;
      let errors = null;
      let code = 500;
      try {
        data = await deletePDSItem(pdsItemID);
        code = 200;
      } catch(err) {
        console.log('got error deleting PDS Item (archiving)', err);
        console.log('id', pdsID);
        errors = [ err.message ];
      }

      return h.response({ data, errors });
    }
  },
  {
    method: 'GET',
    path: '/pds-unassigned/{pdsGroupID?}',
    options: { log: { collect: true }},

    handler: async function(req, h) {

      const { getAllUnassigned, getPDSGroupUnassigned } = methods.questions;
      const pdsGroupID = req.params.pdsGroupID;

      let data = null;
      let errors = null;
      let code = 500;

      try {
        if ( typeof pdsGroupID === 'undefined' || pdsGroupID === null ) {
          data = await getAllUnassigned();
        } else {
          data = await getPDSGroupUnassigned(pdsGroupID);
        }
        code = 200;
      } catch(err) {
        console.error(err);
        errors = [ err.message ];
      }

      return h.response({ data, errors }).code(code);
    }
  },
  {
    method: 'POST',
    path: '/add-pds-other',
    options: { log: { collect: true }},

    handler: async function(req, h) {
      const { addPDSOther } = methods.questions;

      let { id, itemText, isError } = req.payload;

      let data = null;
      let errors = null;
      let code = 500;
      try {
        itemText = itemText.trim();
        // force to int
        isError = (isError) ? 1 : 0;
        data = await addPDSOther(id, itemText, isError);
        code = 200;
      } catch( err ) {
        console.error(err);
        errors = [ err.message ];
      }

      return h.response({data, errors}).code(code);
    }
  },
  {
    method: 'POST',
    path: '/assign-pds-other',
    options: { log: { collect: true }},

    handler: async function(req, h) {
      const { assignEntry } = methods.questions;

      let { otherID, pdsItemID, isError } = req.payload;

      let data = null;
      let errors = null;
      let code = 500;
      try {

        // force to int
        isError = (isError) ? 1 : 0;
        data = await assignEntry(otherID, pdsItemID, isError);
        code = 200;
      } catch( err ) {
        console.error(err);
        errors = [ err.message ];
      }

      return h.response({data, errors}).code(code);
    }
  }

];

  return jobs;
}
