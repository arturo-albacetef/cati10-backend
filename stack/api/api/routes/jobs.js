module.exports = function(methods){
  return [
    {
      method: 'GET',
      path:   '/jobs',
      options: { log: { collect: true }},

      handler: async function(req, h) {
        const headers = [
          { "text": "Job #", "value": "jobno", "align": "left" },
          { "text": "Description", "value": "description", "align": "left" },
          { "text": "Status", "value": "status", "align": "left" },
          { "text": "Notification message", "value": "notification_message", "align": "left" }
        ];

        const { getJobs } = methods.jobs;

        let jobs;
        try {
          jobs = await getJobs();
          jobs = jobs.map( j => Object.assign(j, {
            // props for data table
            'status': (j.state !== null) ? j.state.name : null,
            'bonus_code': (j.bonusCode !== null) ? j.bonusCode.name : null,
          }));
        } catch(err) {
          console.log(err);
          return h.response({data: null, errors: [ err.message ]}).code(500);
        }

        return h.response({
          data: {
            headers: headers,
            rows: jobs || []
          },
          errors: null
        });
      }
    },
    {
      method: 'PUT',
      path: '/jobs/{jobID}',
      options: {log: {collect: true}},

      handler: async function(req, h) {
        const { updateJob } = methods.jobs;

        const jobID = req.params.jobID;
        const jobData = req.payload;

        let code = 500;
        let data = null;
        let errors = null;
        try {
          data = await updateJob(jobID, {
            bonus_code_id     : jobData['bonus_code_id'],
            job_state_id      : jobData['job_state_id'],
            breaking_news     : jobData['breaking_news'],
            jobno             : jobData['jobno'],
            description       : jobData['description'],
            pds_group_id      : jobData['pds_group_id']
          });
          code = 200;
        } catch(error) {
          errors = [ error.message ];
        }

        return h.response({ data, errors }).code(code);
      }
    },
    {
      method: 'GET',
      path: '/job-states',
      options: { log: { collect: true }},

      handler: async function(req, h) {
        const { getJobStates } = methods.jobs;

        let jobStates;
        try {
          jobStates = await getJobStates();
        } catch (error) {
          console.log(error);
          return h.response({data: null, errors: [err]}).code(500);
        }

        return h.response({data: jobStates, errors:null }).code(200);
      }
    }
  ];
}
