#!/bin/sh

# go to app-root
cd /app

# give mysql 10 seconds to connect & kick off server
sleep 10

# kick off server!
su -c 'npm run start' node
