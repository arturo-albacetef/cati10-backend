-- generates a mock day for an agent --
-- they will login, start a job, and go through the steps of a complete call --

SET @record_picked    = 1;
SET @record_served    = 2;
SET @record_started   = 3;
SET @record_abandoned = 4;
SET @record_ended     = 5;
SET @job_started      = 6;
SET @job_ended        = 7;
SET @login            = 8;
SET @logout           = 9;

SET @agent_id         = 28;
SET @interviewing     = (
  SELECT id
  FROM c10_activities
  WHERE codename = 'interviewing'
);

SET @call_id = 5288;
SET @job_id = (SELECT job_id FROM c10_calls WHERE id = @call_id );
SET @contact_id = (SELECT contact_id FROM `c10_calls` WHERE id = @call_id);

-- Create the logging of call events --

INSERT INTO `c10_agent_events`
  ( agent_id, event_type_id, activity_id, date_updated, call_id)
VALUES
  -- agent logs in --
  ( @agent_id, @login, NULL, NOW(), NULL ),

  -- agent starts job --
  ( @agent_id, @job_started,    @interviewing, ADDTIME(NOW(), '00:15:00'), NULL ),

  -- begin call steps --
  (
    @agent_id,
    @record_picked,
    @interviewing,
    ADDTIME(NOW(), '00:30:00'),
    @call_id
  ),
  (
    @agent_id,
    @record_served,
    @interviewing,
    ADDTIME(NOW(), '00:31:00'),
    @call_id
  ),
  (
    @agent_id,
    @record_started,
    @interviewing,
    ADDTIME(NOW(), '00:32:00'),
    @call_id
  ),
  (
    @agent_id,
    @record_ended,
    @interviewing,
    ADDTIME(NOW(), '01:00:00'),
    @call_id
  ),

  -- end job --
  ( @agent_id,
    @job_ended,
    @interviewing,
    ADDTIME(NOW(), '01:05:00'),
    NULL
  ),

  -- logout --
  (
    @agent_id,
    @logout,
    NULL,
    ADDTIME(NOW(), '02:00:00'),
    NULL
  );


-- Record activity start --

-- @NOTE only works in case there is only one of these... --
-- suffices for mock purposes --
SET @event_id = (
  SELECT id
  FROM `c10_agent_events`
  WHERE
    agent_id      = @agent_id     AND
    event_type_id = @job_started  AND
    activity_id   = @interviewing
);

INSERT INTO `c10_agent_activities`
  ( agent_id, activity_id, agent_event_id, job_id, start_time, end_time, records_viewed )
VALUES
  (
    @agent_id,
    @interviewing,
    @event_id,
    @job_id,
    NOW(),
    ADDTIME(NOW(), '02:00:00'),
    1
  );

-- Record activity completion --

-- @NOTE only works if there is only one record of this type... --
-- suffices for mock purposes --
SET @agent_activity_id = (
  SELECT id
  FROM `c10_agent_activities`
  WHERE
    agent_id    = @agent_id     AND
    activity_id = @interviewing AND
    job_id      = @job_id
);

INSERT INTO `c10_activity_completes`
  (agent_activity_id, contact_id)
VALUES
  (@agent_activity_id, @contact_id);
