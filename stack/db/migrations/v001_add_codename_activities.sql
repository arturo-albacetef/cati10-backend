-- this code will add a "machine friendly" codename to activities --

ALTER TABLE c10_activities
ADD COLUMN `codename` TEXT
AFTER `activity`;

-- now update the table with the corresponding codenames --
UPDATE c10_activities
SET `codename` = (
  CASE
    WHEN (`activity` = 'Interviewing (T1)') THEN 'interviewing'
    WHEN (`activity` = 'Testing (T2)')      THEN 'testing'
    WHEN (`activity` = 'Out to Lunch')      THEN 'out_to_lunch'
    WHEN (`activity` = 'Training (T3)' )    THEN 'training'
    WHEN (`activity` = 'Meetings' )         THEN 'meetings'
    ELSE (`activity`)
  END
)
WHERE `activity` in (
  "Interviewing (T1)",
  "Testing (T2)",
  "Out to Lunch",
  "Training (T3)",
  "Meetings"
);
