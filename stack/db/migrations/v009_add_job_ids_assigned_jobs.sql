ALTER TABLE `c10_assigned_jobs`
ADD COLUMN `job_id` INT NULL;


UPDATE `c10_assigned_jobs`
INNER JOIN `c10_jobs`
  ON `c10_jobs`.jobno = `c10_assigned_jobs`.jobno
SET c10_assigned_jobs.job_id = c10_jobs.id;
