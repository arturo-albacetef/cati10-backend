-- stores the actual performing of activities --
CREATE TABLE IF NOT EXISTS `c10_agent_activities` (
  id INT NOT NULL AUTO_INCREMENT,
  agent_id INT NOT NULL,
  activity_id INT NOT NULL,

  -- @NOTE need to put some thought into which event should be tied to this --
  -- maybe nullable field ? --
  agent_event_id INT NOT NULL,
  job_id INT NOT NULL,

  start_time DATETIME NOT NULL,
  end_time DATETIME,
  records_viewed INT DEFAULT 0,

  PRIMARY KEY (id),
  FOREIGN KEY (job_id) REFERENCES c10_jobs(id),
  FOREIGN KEY (agent_id) REFERENCES c10_login(id),
  FOREIGN KEY (agent_event_id) REFERENCES c10_agent_events(id)
) ENGINE=InnoDB;
