
-- this is meant to encapsulate the types of events that an agent performs --

CREATE TABLE `c10_event_types` (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name  TEXT NOT NULL,
  codename TEXT NOT NULL
) ENGINE=InnoDB;

INSERT INTO c10_event_types (name, codename)
VALUES
  ("record picked", "record_picked"),
  ("record served", "record_served"),
  ("record started", "record_started"),
  ("record abandoned", "record_abandoned"),
  ("record ended", "record_ended" ),
  ("job started", "job_started"),
  ("job ended", "job_ended"),
  ("login", "login"),
  ("logout", "logout");
