-- add the missing ID field to c10_login to allow for FK --

ALTER TABLE `c10_login`
ADD COLUMN `id`
  INT NOT NULL AUTO_INCREMENT PRIMARY KEY
FIRST;
