CREATE TABLE IF NOT EXISTS `c10_job_rules_allowed_fields` (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  table_name TEXT NOT NULL,
  column_name TEXT NOT NULL,
  column_comment TEXT NULL,
  is_active TINYINT(1) NOT NULL DEFAULT 1
) Engine=InnoDB;

SET @dbname = "uk_cati";
INSERT INTO `c10_job_rules_allowed_fields`
(table_name, column_name, column_comment)
(
  SELECT TABLE_NAME, COLUMN_NAME, COLUMN_COMMENT
  FROM information_schema.columns
  WHERE
    TABLE_NAME = "c10_contacts" AND
    TABLE_SCHEMA = @dbname AND
    COLUMN_NAME NOT IN ("extlink", "idnum")
);
