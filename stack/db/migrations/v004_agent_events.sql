CREATE TABLE `c10_agent_events` (
  id INT NOT NULL AUTO_INCREMENT,
  agent_id INT NOT NULL,
  event_type_id INT NOT NULL,
  activity_id INT NULL,
  date_updated DATETIME NOT NULL,
  call_id INT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (agent_id) REFERENCES c10_login(id),
  FOREIGN KEY (event_type_id) REFERENCES c10_event_types(id),
  FOREIGN KEY (activity_id) REFERENCES c10_activities(id),
  FOREIGN KEY (call_id) REFERENCES c10_calls(id)
) ENGINE=InnoDB;
