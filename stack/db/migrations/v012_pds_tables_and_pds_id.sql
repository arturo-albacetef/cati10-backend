
-- alter table to add question text --
ALTER TABLE `c10_pds_groups`
ADD COLUMN `text` TEXT NOT NULL;

-- alter table to add job_id --
ALTER TABLE `c10_pds_groups`
ADD COLUMN `job_id` INT NULL;

ALTER TABLE `c10_pds_groups`
ADD CONSTRAINT
  FOREIGN KEY c10_pds_groups(job_id) REFERENCES c10_jobs(id);


-- add foreign key pds_group_id to c10_questions
ALTER TABLE `c10_questions`
ADD COLUMN pds_group_id INT NULL;

ALTER TABLE `c10_questions`
ADD CONSTRAINT
  FOREIGN KEY c10_questions(pds_group_id) REFERENCES c10_pds_groups(id);
