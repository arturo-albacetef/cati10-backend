-- store the completion of activities done by agents --

CREATE TABLE `c10_activity_completes` (
  id INT NOT NULL AUTO_INCREMENT,
  agent_activity_id INT NOT NULL,
  contact_id INT NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (agent_activity_id) REFERENCES c10_agent_activities(id),
  FOREIGN KEY (contact_id) REFERENCES c10_contacts(extlink)
) ENGINE=InnoDB;
