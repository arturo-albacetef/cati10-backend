
ALTER TABLE `c10_login`
ADD COLUMN `agent_role_id` INT NULL;

ALTER TABLE `c10_login`
ADD CONSTRAINT
  FOREIGN KEY c10_login(agent_role_id)
  REFERENCES c10_agent_roles(id);


SET @entry    = (SELECT id FROM c10_agent_roles WHERE role_shortname = "entry" );
SET @manager  = (SELECT id FROM c10_agent_roles WHERE role_shortname = "manager" );
SET @designer = (SELECT id FROM c10_agent_roles WHERE role_shortname = "designer" );
SET @admin    = (SELECT id FROM c10_agent_roles WHERE role_shortname = "admin" );
SET @disabled = (SELECT id FROM c10_agent_roles WHERE role_shortname = "disabled" );
SET @deleted  = (SELECT id FROM c10_agent_roles WHERE role_shortname = "deleted" );

UPDATE `c10_login`
SET `agent_role_id` = (
  CASE
    WHEN userlevel = "0" THEN @entry
    WHEN userlevel = "1" THEN @manager
    WHEN userlevel = "2" THEN @designer
    WHEN userlevel = "3" THEN @admin
    WHEN userlevel = "D" THEN @disabled
    WHEN userlevel = "X" THEN @deleted
  END
)
WHERE userlevel in ("0", "1", "2", "3", "X", "D");
