const env = require('../env.json');

const { db } = env;

const knex = require('knex')({
  client: db['client'],
  connection: {
    host      : db['host'],
    port      : db['port'],
    user      : db['auth']['user'],
    password  : db['auth']['pass'],
    database  : db['name']
  }
});



/*
  @desc: Will pull the list of PDS questions from the DB

  @TODO paginate in the case of a very long list
*/
async function getPDSQuestions() {

  const pdsQuestions = await (
    knex('c10_questions')
      .select('c10_questions.*')
      .join('c10_question_types', 'c10_question_types.id', '=', 'c10_questions.type_id')
      .where('c10_question_types.name', '=', 'pds')
  );

  return pdsQuestions;
}

/*
  @desc: Will attempt to get the pds group with the given name, if not,
          will create it and return the inserted row

  @TODO checking for uniqueness would be a good start.
*/
async function getOrCreatePDSGroup(name, job_id, text) {

  // attempt to get the group
  const pdsGroup = await (
    knex('c10_pds_groups')
      .select()
      .where({'name': name, 'job_id': job_id})
  );

  /* @TODO might want to be stricter on this... */
  if ( pdsGroup.length > 0 ) {
    return pdsGroup;
  }

  // no group - create it!
  if ( typeof text !== 'string' ) {
    throw new Error('no pds text supplied!');
  }

  const insertResult = await (
    knex('c10_pds_groups')
      .insert({name: name, job_id: job_id, text: text})
  );

  return await (
    knex('c10_pds_groups')
      .select()
      .where({ id: insertResult[0] })
  );
}

/*

  @desc: Will parse the definition property into an object, along with the pds
  subproperty, which lists the pds items pertaining to that question.

  @returns: a new version of the row where the definition property has been
  overriden with the object.
*/
function parseDefinition(pdsRow) {
  const definition = JSON.parse(
    decodeURIComponent(pdsRow.definition)
  );
  definition.pds = JSON.parse(definition.pds);

  return Object.assign(pdsRow, { definition });
}

/*
  @desc: Will insert the pds items supplied, with the appropriate pds group
  supplied.


  @TODO handle case where PDS is not an array of strings, but of objects
*/
async function insertPdsItems(pdsGroup, pdsItemList) {
  const rows = pdsItemList
    .map( (text, k) => ({
      group_id: pdsGroup['id'],
      attribute_text: text,
      name: `item_${k}`,
      seq: k + 1
    })
  );
  return await (
    knex
      .insert( rows )
      .into('c10_pds_attributes')
    );
}


async function migratePDS() {
  const p = await getPDSQuestions();
  const pdsQuestions = p.map( (q) => {

    try {
      let def = parseDefinition(q);
      return def;
    } catch(err) {
      console.log('got error', err, q);
      return;
    }
  });

  let k = 1;
  const total = pdsQuestions.length;
  for( let pdsQ of pdsQuestions ) {
    const {job_id, definition} = pdsQ;

    const qname = definition['question'];
    const qtext = definition['caption'];
    const pdsItemList = definition['pds'];
    console.log('definition', definition);


    const pdsGroup = (await getOrCreatePDSGroup(
      qname,
      job_id,
      qtext
    ))[0];

    const pdsItems = await insertPdsItems(pdsGroup, pdsItemList);

    console.log(`migrated ${k}/${total} pds groups`);
    k++;
  }

  console.log('done!');
}

async function migrateObjections() {}

migratePDS();
