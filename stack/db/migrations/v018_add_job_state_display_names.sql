ALTER TABLE `c10_job_states`
ADD COLUMN display_name TEXT NULL;

UPDATE `c10_job_states`
SET display_name = 'Active' WHERE name = 'A';

UPDATE `c10_job_states`
SET display_name = 'Testing' WHERE name = 'T';

UPDATE `c10_job_states`
SET display_name = 'Deleted' WHERE name = 'D';
