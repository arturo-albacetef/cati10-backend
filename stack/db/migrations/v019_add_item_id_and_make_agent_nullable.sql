ALTER TABLE `c10_pds_assignments`
ADD COLUMN `pds_attribute_id` INT NOT NULL;

ALTER TABLE `c10_pds_assignments`
MODIFY COLUMN `agent_id` INT NULL;

ALTER TABLE `c10_pds_assignments`
MODIFY COLUMN `time_assigned` DATETIME;
