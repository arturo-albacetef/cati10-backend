const fs = require('fs');


const env = require('../env.json');
const { db } = env;

const knex = require('knex')({
  client: db['client'],
  connection: {
    host      : db['host'],
    port      : db['port'],
    user      : db['auth']['user'],
    password  : db['auth']['pass'],
    database  : db['name']
  }
});

async function getQuestions() {
  return (
    await knex('c10_questions')
      .join('c10_question_types', 'c10_questions.type_id', 'c10_question_types.id')
      .select('c10_question_types.name as type_name', 'c10_questions.*')
    );
}

function parseDefinition(definition) {
  const def = JSON.parse(decodeURIComponent(definition));
  const pdsProp = JSON.parse(def.pds);

  return Object.assign({}, def, { pds: pdsProp });
}

async function getQuestionTypes() {
  return (await knex('c10_question_types').select());
}

function parseQuestion(row) {
  const pdsType = questionTypes.find( item => item['type_name'] === 'pds' );
  const isPds = (row['type_id'] === pdsType['id']);
  return is
}


async function main() {
  const questions = await getQuestions();

  let badQuestions = [];
  let k = 0;
  // const pdsQuestions = questions.filter( row => row['type_name'] === 'pds' );
  for(let q of questions) {
    let row;
    try {
      row = decodeURIComponent(q['definition']);
    } catch(err) {
      console.log('error', err.stack);
      badQuestions.push([k, q, err.stack]);
    }
    k++;
  }

  console.log('number of questions', questions.length);
  console.log('number of bad questions', badQuestions.length);

  fs.writeFileSync(
    `bad-rows-${Date.now()}.json`,
    JSON.stringify(badQuestions, null, 2),
    { encoding: 'utf8' }
  );

}


main();
