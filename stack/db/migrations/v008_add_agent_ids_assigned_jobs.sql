ALTER TABLE `c10_assigned_jobs`
ADD COLUMN `agent_id` INT NULL;


UPDATE `c10_assigned_jobs`
INNER JOIN `c10_login`
  ON `c10_login`.name = `c10_assigned_jobs`.name
SET c10_assigned_jobs.agent_id = c10_login.id;
