
-- make table uniform --

UPDATE `c10_pds_attributes`
  SET attribute_text = name
  WHERE attribute_text IS NULL;
