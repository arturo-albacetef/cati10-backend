CREATE TABLE IF NOT EXISTS `c10_agent_roles` (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  role_shortname TEXT NOT NULL,
  role_name TEXT NOT NULL
) Engine=InnoDB;

INSERT INTO `c10_agent_roles` (role_shortname, role_name)
VALUES
  ("entry", "Entry"),
  ("manager", "Manager"),
  ("designer", "Designer"),
  ("admin", "Administrator"),

  -- @TODO these two should be put as states of c10_login rows --
  ("disabled", "Disabled (D)"),
  ("deleted", "Deleted (X)")
;
