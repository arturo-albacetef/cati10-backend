CREATE TABLE IF NOT EXISTS `c10_pds_unassigned` (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  pds_group_id INT NOT NULL,
  call_id INT NOT NULL,
  date_inserted DATE NOT NULL,
  item_text TEXT NOT NULL,
  is_assigned TINYINT(1) DEFAULT 0,

  FOREIGN KEY (pds_group_id) REFERENCES c10_pds_groups(id),
  FOREIGN KEY (call_id) REFERENCES c10_calls(id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `c10_pds_assignments` (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  pds_unassigned_id INT NOT NULL,
  time_assigned DATE NOT NULL,
  agent_id INT NOT NULL,
  is_error TINYINT(1) DEFAULT 0,

  FOREIGN KEY (pds_unassigned_id) REFERENCES c10_pds_unassigned(id),
  FOREIGN KEY (agent_id) REFERENCES c10_login(id)
) ENGINE=InnoDB;
