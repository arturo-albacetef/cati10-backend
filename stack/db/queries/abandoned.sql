SET @record_abandoned = (
  SELECT id
  FROM c10_event_types
  WHERE codename = "record_abandoned"
);

SELECT
  events.id,
  events.agent_id,
  events.event_type_id,
  events.activity_id,
  events.date_updated,
  events.call_id
FROM c10_agent_events AS events
WHERE
  events.event_type_id = @record_abandoned;
