-- store event IDs --
SET @record_ended = (
  SELECT id
  FROM c10_event_types
  WHERE codename = "record_ended"
);

-- select all finished but not completed records --
SELECT *
FROM c10_agent_events AS events
INNER JOIN
  c10_agent_activities
  ON c10_agent_activities.agent_event_id = events.id
LEFT JOIN
  c10_activity_completes
  ON c10_agent_activities.id = c10_activity_completes.agent_activity_id
WHERE
  events.event_type_id = @record_ended
;
