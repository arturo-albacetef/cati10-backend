-- store event IDs --
SET @record_served = (
  SELECT id
  FROM c10_event_types
  WHERE codename = "record_served"
);
SET @record_started = (
  SELECT id
  FROM c10_event_types
  WHERE codename = "record_started"
);

-- query all events with served records where the record was not started --
SELECT
  events.id,
  events.agent_id,
  events.event_type_id,
  events.activity_id,
  events.date_updated,
  events.call_id
FROM c10_agent_events AS events
WHERE
  events.event_type_id = @record_served
AND (
  (events.agent_id, events.call_id) NOT IN (
    SELECT agent_id, call_id
    FROM c10_agent_events
    WHERE event_type_id = @record_started
  )
);
